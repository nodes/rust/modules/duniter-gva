//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use duniter_core::crypto::keys::ed25519::PublicKey;
use duniter_core::crypto::keys::PublicKey as _;
use duniter_core::dbs::{bincode_db, IdtyDbV2, WalletConditionsV2};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct WalletCursor(WalletScriptV10);
impl WalletCursor {
    pub fn from_ref(script: &WalletScriptV10) -> &Self {
        #[allow(trivial_casts)]
        unsafe {
            &*(script as *const WalletScriptV10 as *const WalletCursor)
        }
    }
}

impl Cursor for WalletCursor {}

impl Default for WalletCursor {
    fn default() -> Self {
        WalletCursor(WalletScriptV10::single_sig(PublicKey::default()))
    }
}

impl std::fmt::Display for WalletCursor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.to_string())
    }
}

impl FromStr for WalletCursor {
    type Err = WrongCursor;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(pubkey) = PublicKey::from_base58(s) {
            Ok(WalletCursor(WalletScriptV10::single_sig(pubkey)))
        } else if let Ok(wallet_script) = duniter_core::documents_parser::wallet_script_from_str(s)
        {
            Ok(WalletCursor(wallet_script))
        } else {
            Err(WrongCursor)
        }
    }
}

impl Ord for WalletCursor {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        use bincode::config::Options as _;
        let self_bin = bincode_db()
            .serialize(&self.0)
            .unwrap_or_else(|_| unreachable!());
        let other_bin = bincode_db()
            .serialize(&other.0)
            .unwrap_or_else(|_| unreachable!());
        self_bin.cmp(&other_bin)
    }
}

impl From<WalletCursor> for WalletConditionsV2 {
    fn from(val: WalletCursor) -> Self {
        WalletConditionsV2(val.0)
    }
}

impl PartialOrd for WalletCursor {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use bincode::config::Options as _;
        let self_bin = bincode_db()
            .serialize(&self.0)
            .unwrap_or_else(|_| unreachable!());
        let other_bin = bincode_db()
            .serialize(&other.0)
            .unwrap_or_else(|_| unreachable!());
        self_bin.partial_cmp(&other_bin)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PublicKeyWithBalance(pub PublicKey, pub SourceAmount);
impl AsRef<PubKeyCursor> for PublicKeyWithBalance {
    fn as_ref(&self) -> &PubKeyCursor {
        PubKeyCursor::from_ref(&self.0)
    }
}

#[derive(Debug)]
pub struct ScriptWithBalance(pub WalletScriptV10, pub SourceAmount);
impl AsRef<WalletCursor> for ScriptWithBalance {
    fn as_ref(&self) -> &WalletCursor {
        WalletCursor::from_ref(&self.0)
    }
}

#[derive(Debug)]
pub struct WalletSingleSigWithIdtyOpt(pub PublicKeyWithBalance, pub Option<IdtyDbV2>);

#[derive(Debug)]
pub struct WalletWithIdtyOpt(pub ScriptWithBalance, pub Option<IdtyDbV2>);

impl DbsReaderImpl {
    pub(super) fn wallets_(
        &self,
        exclude_single_sig: bool,
        min_balance_opt: Option<SourceAmount>,
        page_info: PageInfo<WalletCursor>,
    ) -> KvResult<PagedData<Vec<ScriptWithBalance>>> {
        if let Some(min_balance) = min_balance_opt {
            if exclude_single_sig {
                self.wallets_inner(
                    |(k, v)| {
                        if !k.0.is_single_sig() && v.0 >= min_balance {
                            Some(ScriptWithBalance(k.0, v.0))
                        } else {
                            None
                        }
                    },
                    page_info,
                )
            } else {
                self.wallets_inner(
                    |(k, v)| {
                        if v.0 >= min_balance {
                            Some(ScriptWithBalance(k.0, v.0))
                        } else {
                            None
                        }
                    },
                    page_info,
                )
            }
        } else if exclude_single_sig {
            self.wallets_inner(
                |(k, v)| {
                    if !k.0.is_single_sig() {
                        Some(ScriptWithBalance(k.0, v.0))
                    } else {
                        None
                    }
                },
                page_info,
            )
        } else {
            self.wallets_inner(|(k, v)| Some(ScriptWithBalance(k.0, v.0)), page_info)
        }
    }

    pub(super) fn wallets_with_idty_opt_(
        &self,
        bc_db: &BcV2DbRo<FileBackend>,
        min_balance_opt: Option<SourceAmount>,
        page_info: PageInfo<WalletCursor>,
    ) -> KvResult<PagedData<Vec<WalletWithIdtyOpt>>> {
        let paged_data = self.wallets_(false, min_balance_opt, page_info)?;

        let mut data = Vec::with_capacity(paged_data.data.len());
        for script_with_balance in paged_data.data {
            let idty_opt = if let Some(pubkey) = script_with_balance.0.as_single_sig() {
                bc_db.identities().get(&PubKeyKeyV2(pubkey))?
            } else {
                None
            };

            data.push(WalletWithIdtyOpt(script_with_balance, idty_opt));
        }

        Ok(PagedData {
            data,
            has_next_page: paged_data.has_next_page,
            has_previous_page: paged_data.has_previous_page,
        })
    }

    pub(super) fn wallets_single_sig_(
        &self,
        min_balance_opt: Option<SourceAmount>,
        page_info: PageInfo<PubKeyCursor>,
    ) -> KvResult<PagedData<Vec<PublicKeyWithBalance>>> {
        if let Some(min_balance) = min_balance_opt {
            self.wallets_inner(
                |(k, v)| {
                    if v.0 >= min_balance {
                        k.0.as_single_sig().map(|pk| PublicKeyWithBalance(pk, v.0))
                    } else {
                        None
                    }
                },
                page_info,
            )
        } else {
            self.wallets_inner(
                |(k, v)| k.0.as_single_sig().map(|pk| PublicKeyWithBalance(pk, v.0)),
                page_info,
            )
        }
    }

    pub(super) fn wallets_single_sig_with_idty_opt_(
        &self,
        bc_db: &BcV2DbRo<FileBackend>,
        min_balance_opt: Option<SourceAmount>,
        page_info: PageInfo<PubKeyCursor>,
    ) -> KvResult<PagedData<Vec<WalletSingleSigWithIdtyOpt>>> {
        let paged_data = self.wallets_single_sig_(min_balance_opt, page_info)?;

        let mut data = Vec::with_capacity(paged_data.data.len());
        for pk_with_balance in paged_data.data {
            let idty_opt = bc_db.identities().get(&PubKeyKeyV2(pk_with_balance.0))?;
            data.push(WalletSingleSigWithIdtyOpt(pk_with_balance, idty_opt));
        }

        Ok(PagedData {
            data,
            has_next_page: paged_data.has_next_page,
            has_previous_page: paged_data.has_previous_page,
        })
    }

    fn wallets_inner<C, E, F>(
        &self,
        filter_map: F,
        page_info: PageInfo<C>,
    ) -> KvResult<PagedData<Vec<E>>>
    where
        C: Cursor + Into<WalletConditionsV2>,
        E: AsRef<C> + std::fmt::Debug + Send + Sync,
        F: Copy + Fn((WalletConditionsV2, SourceAmountValV2)) -> Option<E>,
    {
        let first_cursor_opt = if page_info.not_all() {
            self.0
                .balances()
                .iter(.., |it| it.filter_map_ok(filter_map).next_res())?
                .map(|element| element.as_ref().to_owned())
        } else {
            None
        };
        let last_cursor_opt = if page_info.not_all() {
            self.0
                .balances()
                .iter_rev(.., |it| it.filter_map_ok(filter_map).next_res())?
                .map(|element| element.as_ref().to_owned())
        } else {
            None
        };

        let cursor_opt = page_info.pos.clone();
        let data = if page_info.order {
            let first_key = cursor_opt
                .unwrap_or_else(|| first_cursor_opt.clone().unwrap_or_default())
                .into();
            self.0.balances().iter(first_key.., |it| {
                if let Some(limit) = page_info.limit_opt {
                    it.filter_map_ok(filter_map)
                        .take(limit.get())
                        .collect::<KvResult<Vec<_>>>()
                } else {
                    it.filter_map_ok(filter_map).collect::<KvResult<Vec<_>>>()
                }
            })?
        } else {
            let last_key = cursor_opt
                .unwrap_or_else(|| last_cursor_opt.clone().unwrap_or_default())
                .into();
            self.0.balances().iter_rev(..=last_key, |it| {
                if let Some(limit) = page_info.limit_opt {
                    it.filter_map_ok(filter_map)
                        .take(limit.get())
                        .collect::<KvResult<Vec<_>>>()
                } else {
                    it.filter_map_ok(filter_map).collect::<KvResult<Vec<_>>>()
                }
            })?
        };

        let page_not_reversed = page_info.order;

        Ok(PagedData {
            has_next_page: if page_info.order {
                has_next_page(
                    data.iter()
                        .map(|element| OwnedOrRef::Borrow(element.as_ref())),
                    last_cursor_opt,
                    page_info.clone(),
                    page_not_reversed,
                )
            } else {
                // Server can't efficiently determine hasNextPage in DESC order
                false
            },
            has_previous_page: if page_info.order {
                // Server can't efficiently determine hasPreviousPage in ASC order
                false
            } else {
                has_previous_page(
                    data.iter()
                        .map(|element| OwnedOrRef::Borrow(element.as_ref())),
                    first_cursor_opt,
                    page_info,
                    page_not_reversed,
                )
            },
            data,
        })
    }
}
