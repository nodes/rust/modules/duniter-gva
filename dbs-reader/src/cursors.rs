//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use duniter_core::crypto::keys::ed25519::PublicKey;
use duniter_core::crypto::keys::PublicKey as _;
use duniter_core::dbs::WalletConditionsV2;

#[derive(Clone, Copy, Debug)]
pub struct WrongCursor;
impl std::fmt::Display for WrongCursor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "wrong cursor")
    }
}
impl std::error::Error for WrongCursor {}

pub trait Cursor:
    'static + Clone + std::fmt::Debug + std::fmt::Display + Default + FromStr + Ord
{
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub struct PubKeyCursor(pub PublicKey);

impl PubKeyCursor {
    pub fn from_ref(pk: &PublicKey) -> &Self {
        #[allow(trivial_casts)]
        unsafe {
            &*(pk as *const PublicKey as *const PubKeyCursor)
        }
    }
}

impl Cursor for PubKeyCursor {}

impl std::fmt::Display for PubKeyCursor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.to_string())
    }
}

impl FromStr for PubKeyCursor {
    type Err = WrongCursor;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(pk) = PublicKey::from_base58(s) {
            Ok(PubKeyCursor(pk))
        } else {
            Err(WrongCursor)
        }
    }
}

impl From<PubKeyCursor> for WalletConditionsV2 {
    fn from(val: PubKeyCursor) -> Self {
        WalletConditionsV2(WalletScriptV10::single_sig(val.0))
    }
}

impl Ord for PubKeyCursor {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.as_ref().cmp(other.0.as_ref())
    }
}

impl PartialOrd for PubKeyCursor {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.as_ref().partial_cmp(other.0.as_ref())
    }
}
