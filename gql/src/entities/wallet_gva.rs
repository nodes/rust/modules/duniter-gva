//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

#[derive(Clone, Copy, async_graphql::Enum, Eq, PartialEq)]
pub(crate) enum WalletTypeFilter {
    /// All wallets
    All,
    /// Exclude wallets scripts with single SIG condition
    OnlyComplex,
    /// Only wallets scripts with single SIG condition
    OnlySimple,
}
impl Default for WalletTypeFilter {
    fn default() -> WalletTypeFilter {
        WalletTypeFilter::OnlySimple
    }
}

#[derive(Clone, Debug, async_graphql::SimpleObject)]
pub(crate) struct Wallet {
    /// Wallet script or public key
    pub(crate) script: String,
    /// Wallet balance
    pub(crate) balance: AmountWithBase,
    /// Optional identity attached to this wallet
    pub(crate) idty: Option<Identity>,
}
