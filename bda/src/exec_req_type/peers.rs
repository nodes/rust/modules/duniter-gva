//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use duniter_core::bda_types::peer::Peer;

pub(super) async fn exec_req_peers_v1(
    bda_executor: &BdaExecutor,
    n: usize,
) -> Result<BcaRespTypeV0, ExecReqTypeError> {
    let dbs_reader = bda_executor.dbs_reader();

    Ok(BcaRespTypeV0::PeersV10(
        bda_executor
            .dbs_pool
            .execute(move |shared_dbs| dbs_reader.get_some_peers(&shared_dbs.dunp_db, n))
            .await??
            .into_iter()
            .map(|peer_db| Peer {
                peer: peer_db.peer,
                is_member: peer_db.member,
                is_up: peer_db.status,
            })
            .collect(),
    ))
}
