//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use duniter_core::bda_types::peer::Peer;

use duniter_gva_db::BLOCKS_CHUNK_SIZE;

pub(super) async fn exec_req_compressed_block_chunk(
    bda_executor: &BdaExecutor,
    chunk_id: usize,
) -> Result<BcaRespTypeV0, ExecReqTypeError> {
    if let Some(ref profile_path) = bda_executor.profile_path_opt {
        let chunks_folder_path = profile_path.join("data/gva_v1_blocks_chunks");
        if let Some(chunk_data) = duniter_gva_dbs_reader::blocks_chunks::read_compressed_chunk(
            chunk_id as u32,
            &chunks_folder_path,
            false,
        )? {
            Ok(BcaRespTypeV0::CompressedBlockChunk {
                chunk_id,
                compressed_data: chunk_data,
            })
        } else {
            Err("not found".into())
        }
    } else {
        Err("Cannot get compressed block chunk in memony mode".into())
    }
}

pub(super) async fn exec_req_sync(
    bda_executor: &BdaExecutor,
    from: u32,
    to_opt: Option<u32>,
) -> Result<BcaRespTypeV0, ExecReqTypeError> {
    let (target_blockstamp, last_chunk_id_opt) = if let Some(to) = to_opt {
        if to < from {
            return Err("to < from".into());
        }

        let dbs_reader = bda_executor.dbs_reader();
        if let Some(block_meta) = bda_executor
            .dbs_pool
            .execute(move |shared_dbs| dbs_reader.block(&shared_dbs.bc_db_ro, U32BE(to)))
            .await??
        {
            (block_meta.blockstamp(), Some(to / BLOCKS_CHUNK_SIZE))
        } else {
            return Err("Not enough blocks, try another server".into());
        }
    } else if let Some(current_meta) = bda_executor.cm_accessor.get_current_meta(|cm| *cm).await {
        (current_meta.current_block_meta.blockstamp(), None)
    } else {
        return Err("no blockchain".into());
    };

    let first_chunk_id = (from + 1) / BLOCKS_CHUNK_SIZE;

    let dbs_reader = bda_executor.dbs_reader();
    let (peers, blocks_chunks_hashs) = bda_executor
        .dbs_pool
        .execute(move |shared_dbs| {
            Ok::<_, KvError>((
                dbs_reader
                    .get_some_peers(&shared_dbs.dunp_db, 0)?
                    .into_iter()
                    .map(|peer_db| Peer {
                        peer: peer_db.peer,
                        is_member: peer_db.member,
                        is_up: peer_db.status,
                    })
                    .collect(),
                dbs_reader.blocks_chunks_hashs(first_chunk_id, last_chunk_id_opt)?,
            ))
        })
        .await??;

    Ok(BcaRespTypeV0::Sync {
        blocks_chunks_hashs,
        peers,
        target_blockstamp,
    })
}
